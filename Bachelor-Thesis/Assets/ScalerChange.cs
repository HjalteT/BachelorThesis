using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class ScalerChange : MonoBehaviour
{
    // Start is called before the first frame update
    
    public void Enlargen()
    {
        this.transform.localScale = new Vector3(1f, 1f, 2f);
    }
    public void Shrink()
    {
        this.transform.localScale = new Vector3(1f, 1f, 0.5f);
    }
    public void resetPosition()
    {
        this.transform.localScale = new Vector3(1f, 1f, 1f);
    }

}
