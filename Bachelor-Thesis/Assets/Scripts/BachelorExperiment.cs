using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

/// <summary>
/// BachelorExperiment.class
/// 
/// version 1.0
/// date: August 2023
/// authors: Hjalte Tromborg
/// </summary>

public class BachelorExperiment : MonoBehaviour
{
	public GameObject _Room1;
	public GameObject _Room2;
	public GameObject _Room3;
	
	public GameObject _Player;		
	private Vector3 playerPosition;
	private float xPos;
	private float yPos;
	private float zPos;

    private bool aButtonPressed = false;
    private static bool created = false;
	private static int ActiveRoom = 0;
	
	// DEBUG
	private bool debug = false;
	private DataLogger data_loggerGameObj;
	private Vector3 gObjPosition;
	private DataLogger data_loggerR1S1; 	
	private DataLogger data_loggerR2S1; 	
	private DataLogger data_loggerR2S2;
	private DataLogger data_loggerR3S1; 	
	private DataLogger data_loggerR3S2; 	
	private DataLogger data_loggerR1L1; 
	private DataLogger data_loggerR1L2; 
	private DataLogger data_loggerR1L3; 	
	private DataLogger data_loggerR2L1; 
	private DataLogger data_loggerR2L2; 
	private DataLogger data_loggerR2L3; 	
	private DataLogger data_loggerR3L1;
	private DataLogger data_loggerR3L2; 
	private DataLogger data_loggerR3L3; 
	private GameObject activeObject;	
	private string gFindTag;
	private string gObjTag;
	private int gTagId;
	private MeshRenderer renderer;
	private Vector3 rSIZE;
	private Vector3 rMIN;
	private Vector3 rMAX;
	
	// Start is called before the first frame update
	void Start()
	{		
		// If debug is true, log position of all important gameobjects.
		if (debug)
			LogGameObjectsPostion();
	
		// Disable all lights
		// Room 1
		activeObject = GameObject.FindGameObjectWithTag("R1L1");
		if (activeObject != null)
			activeObject.SetActive(false);		
		activeObject = GameObject.FindGameObjectWithTag("R1L2");
		if (activeObject != null)
			activeObject.SetActive(false);	
		activeObject = GameObject.FindGameObjectWithTag("R1L3");
		if (activeObject != null)
			activeObject.SetActive(false);	
		// Room 2
		activeObject = GameObject.FindGameObjectWithTag("R2L1");
		if (activeObject != null)
			activeObject.SetActive(false);		
		activeObject = GameObject.FindGameObjectWithTag("R2L2");
		if (activeObject != null)
			activeObject.SetActive(false);	
		activeObject = GameObject.FindGameObjectWithTag("R2L3");
		if (activeObject != null)
			activeObject.SetActive(false);			
		// Room 3
		activeObject = GameObject.FindGameObjectWithTag("R3L1");
		if (activeObject != null)
			activeObject.SetActive(false);		
		activeObject = GameObject.FindGameObjectWithTag("R3L2");
		if (activeObject != null)
			activeObject.SetActive(false);	
		activeObject = GameObject.FindGameObjectWithTag("R3L3");
		if (activeObject != null)
			activeObject.SetActive(false);	
		
		_Room1.SetActive(true);
		_Room2.SetActive(false);
		_Room3.SetActive(false);
		ActiveRoom = 1;
	}

	void LogGameObjectsPostion()
	{
		// Room 1
		gFindTag = "R1S1";
		gTagId = 111;	// Room1=1 / Stick=1 / StickNo=1
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	
		
//		Debug.Log("HT: " + rMIN + " " + rMAX);
//		Debug.Log("HT: " + rSIZE);		

		
		gFindTag = "R1L1";
		gTagId = 121;	// Room1=1 / Light=2 / LightNo=1
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	

		gFindTag = "R1L2";
		gTagId = 122;	// Room1=1 / Light=2 / LightNo=2
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	

		gFindTag = "R1L3";
		gTagId = 123;	// Room1=1 / Light=2 / LightNo=3 
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();			

		// Room 2
		gFindTag = "R2S1";
		gTagId = 211;	// Room1=2 / Stick=1 / StickNo=1
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	
		
		gFindTag = "R2S2";
		gTagId = 212;	// Room1=2 / Stick=1 / StickNo=2
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();		

		gFindTag = "R2L1";
		gTagId = 221;	// Room1=2 / Light=2 / LightNo=1
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	

		gFindTag = "R2L2";
		gTagId = 222;	// Room1=2 / Light=2 / LightNo=2
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	

		gFindTag = "R2L3";
		gTagId = 223;	// Room1=2 / Light=2 / LightNo=3 
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	
		
		// Room 3
		gFindTag = "R3S1";
		gTagId = 311;	// Room1=3 / Stick=1 / StickNo=1
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	
		
		gFindTag = "R3S2";
		gTagId = 312;	// Room1=3 / Stick=1 / StickNo=2
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	

		gFindTag = "R3L1";
		gTagId = 321;	// Room1=3 / Light=2 / LightNo=1
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	

		gFindTag = "R3L2";
		gTagId = 322;	// Room1=3 / Light=2 / LightNo=2
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();	

		gFindTag = "R3L3";
		gTagId = 323;	// Room1=3 / Light=2 / LightNo=3 
		activeObject = GameObject.FindGameObjectWithTag(gFindTag);
		gObjPosition = activeObject.transform.position;
		gObjTag = "GameObjPos_" + gFindTag;
		renderer = activeObject.GetComponent<MeshRenderer>();
		rSIZE = renderer.bounds.size;
		rMIN = renderer.bounds.min;
		rMAX = renderer.bounds.max;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (gObjPosition.x).ToString(), (gObjPosition.y).ToString(), (gObjPosition.z).ToString());
		data_loggerGameObj.StopLogging();
		gObjTag = "GameObjSize_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rSIZE.x).ToString(), (rSIZE.y).ToString(), (rSIZE.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMIN_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMIN.x).ToString(), (rMIN.y).ToString(), (rMIN.z).ToString());
		data_loggerGameObj.StopLogging();		
		gObjTag = "GameObjSizeMAX_" + gFindTag;
		data_loggerGameObj = new DataLogger(";", gObjTag + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
		data_loggerGameObj.StartLogging();			
		data_loggerGameObj.Log(0, gTagId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), gObjTag, (rMAX.x).ToString(), (rMAX.y).ToString(), (rMAX.z).ToString());
		data_loggerGameObj.StopLogging();		
	}
	
	IEnumerator waiter()
	{
		//Wait for 1 seconds
		Debug.Log("Room Change - Add button delay...");
		yield return new WaitForSecondsRealtime(2);
		aButtonPressed = false;			
	}
	
    public void move2Room()
    {
        if (aButtonPressed == true)
        {
			ActiveRoom = 0;
			if (_Room1.activeInHierarchy == true) {
				xPos = 2.57f;
				yPos = 1.862f;
				zPos = 22.905f;
				ActiveRoom = 1;
				Debug.Log("Room1->Room2 -> " + ActiveRoom);
			}
			if (_Room2.activeInHierarchy == true) {
				xPos = -7.71f;
				yPos = 1.862f;
				zPos = 22.905f;
				ActiveRoom = 2;
				Debug.Log("Room2->Room3 -> " + ActiveRoom);
			}
			if (_Room3.activeInHierarchy == true) {
				xPos = 13.35561f;
				yPos = 1.862f;
				zPos = 22.905f;
				ActiveRoom = 3;
				Debug.Log("Room3->Room1 -> " + ActiveRoom);
			}
			if (ActiveRoom > 0) {
				Debug.Log("Room Activation: -> " + ActiveRoom);
				_Player.SetActive(false);
				if (ActiveRoom == 1) {
					Debug.Log("Activating room 2: -> " + ActiveRoom);
					_Room1.SetActive(false);
					_Room2.SetActive(true);
					_Room3.SetActive(false);	
				}
				if (ActiveRoom == 2) {
					Debug.Log("Activating room 3: -> " + ActiveRoom);
					_Room1.SetActive(false);
					_Room2.SetActive(false);
					_Room3.SetActive(true);	
				}
				if (ActiveRoom == 3) {
					Debug.Log("Activating room 1: -> " + ActiveRoom);
					_Room1.SetActive(true);
					_Room2.SetActive(false);
					_Room3.SetActive(false);				
				}
				Debug.Log("Room: Moving Player to new pos.");
				Vector3 newPos = new Vector3(xPos,yPos,zPos);
				_Player.transform.position = newPos;
				_Player.SetActive(true);
				
				StartCoroutine(waiter());
			}
		}		
	}
	
	// Update is called once per frame
	void Update()
	{					
		OVRInput.Update(); // Call before checking the input
        if (OVRInput.Get(OVRInput.RawButton.A) && !aButtonPressed)
        {
            aButtonPressed = true;
			move2Room();		
		}
	}
}
