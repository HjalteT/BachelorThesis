using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneB_Q2 : MonoBehaviour
{
    private static bool created = false;
	private bool aButtonPressed = false;

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
            Debug.Log("Awake BCL: " + this.gameObject);
        }
    }
	
	IEnumerator waiter()
	{
		//Wait for 1 seconds
		yield return new WaitForSecondsRealtime(1);	
		aButtonPressed = false;	
	}

    public void LoadScene()
    {
		if (aButtonPressed == true)
        {
			if (SceneManager.GetActiveScene().name == "BCLScene")
			{
				//SceneManager.LoadScene("Q2", LoadSceneMode.Single);
				SceneManager.LoadScene("Q2");
			}
		}
    }
	
	// Update is called once per frame
    private void Update()
    {
		OVRInput.Update(); // Call before checking the input
        if (OVRInput.Get(OVRInput.RawButton.B) && !aButtonPressed)
        {
            aButtonPressed = true;
			LoadScene();
			StartCoroutine(waiter());
		}
	}
}