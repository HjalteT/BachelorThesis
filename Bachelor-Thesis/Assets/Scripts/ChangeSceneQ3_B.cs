using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;


namespace VRQuestionnaireToolkit
{
	public class ChangeSceneQ3_B : MonoBehaviour
	{
		private static bool created = false;
		private ExportToCSV _exportToCsvScript;
		private GameObject _exportToCsv;
		private bool aButtonPressed = false;
			 
		void Start()
		{
		   _exportToCsv = GameObject.FindGameObjectWithTag("ExportToCSV");
		   _exportToCsvScript = _exportToCsv.GetComponent<ExportToCSV>();
		   _exportToCsvScript.QuestionnaireFinishedEvent.AddListener(AllowLoadScene); // e.g, call next questionnaire from list
		}
		
		void AllowLoadScene()
		{
			created = true;
		}
			
		void Awake()
		{
			if (!created)
			{
				//DontDestroyOnLoad(this.gameObject);
				created = false;
				Debug.Log("Awake Q3: " + this.gameObject);
			}
		}
		
		IEnumerator waiter()
		{
			//Wait for 1 seconds
			yield return new WaitForSecondsRealtime(1);	
			aButtonPressed = false;	
		}
		
		public void LoadScene()
		{
			if (aButtonPressed == true)
			{
				if (SceneManager.GetActiveScene().name == "Q3")
				{
					//SceneManager.LoadScene("BCLScene", LoadSceneMode.Single);
					SceneManager.LoadScene("BCLScene");
				}
			}
		}
		
		// Update is called once per frame
		void Update()
		{
			OVRInput.Update(); // Call before checking the input
			if (OVRInput.Get(OVRInput.RawButton.B) && created == true && !aButtonPressed)
			{
				aButtonPressed = true;
				created = false;
				LoadScene();
				StartCoroutine(waiter());
			}
		}
	}
}