using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControllerLogging : MonoBehaviour
{	
/////////////////////////////////////////////////	
	// CUSTUMIZE BEFORE EXECUTION !!!
	private int candidateId = 1;	// Test person ID number
	private int experimentId = 1; 	// ID=1: non-CRI without Stick2, ID=2: CRI without Stick2, ID=3: non-CRI with Stick2, ID=4: CRI with Stick2
/////////////////////////////////////////////////	
	
    private DataLogger data_logger1; 
	private DataLogger data_logger2; 
	private DataLogger data_logger3; 
	private DataLogger data_logger4; 
	private DataLogger data_logger5; 
	private DataLogger data_logger6; 
	private bool aButtonPressed = false;
	private string eventName;
		
	public GameObject gazePointer;
	private Vector3 hitPosition1;
	private Vector3 hitPosition2;
	private Vector3 hitPosition3;
	
	private float hitDistance;
	private Vector3 hitTransPosition;
	private Vector3 gazePoint; // Stores the point where the user is looking
	
	// Cursor (pointer)
	public GameObject cursorPrefab;
    public float maxCursorDistance = 30;
    private GameObject cursorInstance;
	
	private int tabLocation = 1;

	Ray ray;
	
    void Start()
    {   
		cursorInstance = Instantiate(cursorPrefab);	
		// Ray ray = new Ray(gazePointer.transform.position, gazePointer.transform.forward);
		Ray ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
    }
	
	IEnumerator waiter()
	{
		//Wait for 1 seconds
		Debug.Log("Stick (Wait for 1 seconds");
		yield return new WaitForSecondsRealtime(1);	
		aButtonPressed = false;	
		tabLocation++;
	}
	
	void Write2Log1(string tag, Vector3 hitPos, float hitDis, int tabLoc)
	{
		if (aButtonPressed == true)
        {
			eventName = tag + "_Distance_" + hitDis + "_P" + tabLoc.ToString() + "_Position";
			data_logger1.Log(candidateId, experimentId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), eventName, 
				(hitPos.x).ToString(),
				(hitPos.y).ToString(),
				(hitPos.z).ToString());
			data_logger1.StopLogging();
		}		
	}	
	
 	void Write2Log2(string tag, Vector3 hitPos, float hitDis, int tabLoc)
	{
		if (aButtonPressed == true)
        {
			eventName = tag + "_Distance_" + hitDis + "_P" + tabLoc.ToString() + "_Position";
			data_logger2.Log(candidateId, experimentId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), eventName,  
				(hitPos.x).ToString(),
				(hitPos.y).ToString(),
				(hitPos.z).ToString());
			data_logger2.StopLogging();
		}		
	}

 	void Write2Log3(string tag, Vector3 hitPos, float hitDis, int tabLoc)
	{
		if (aButtonPressed == true)
        {
			eventName = tag + "_Distance_" + hitDis + "_P" + tabLoc.ToString() + "_Position";
			data_logger3.Log(candidateId, experimentId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), eventName, 
				(hitPos.x).ToString(),
				(hitPos.y).ToString(),
				(hitPos.z).ToString());
			data_logger3.StopLogging();
		}		
	}
	
 	void Write2Log4(string tag, Vector3 hitPos, float hitDis, int tabLoc)
	{
		if (aButtonPressed == true)
        {
			eventName = tag + "_Distance_" + hitDis + "_P" + tabLoc.ToString() + "_Position";
			data_logger4.Log(candidateId, experimentId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), eventName, 
				(hitPos.x).ToString(),
				(hitPos.y).ToString(),
				(hitPos.z).ToString());
			data_logger4.StopLogging();
		}		
	}

 	void Write2Log5(string tag, Vector3 hitPos, float hitDis, int tabLoc)
	{
		if (aButtonPressed == true)
        {
			eventName = tag + "_Distance_" + hitDis + "_P" + tabLoc.ToString() + "_Position";
			data_logger5.Log(candidateId, experimentId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), eventName, 
				(hitPos.x).ToString(),
				(hitPos.y).ToString(),
				(hitPos.z).ToString());
			data_logger5.StopLogging();
		}		
	}
	
 	void Write2Log6(string tag, Vector3 hitPos, float hitDis, int tabLoc)
	{
		if (aButtonPressed == true)
        {
			eventName = tag + "_Distance_" + hitDis + "_P" + tabLoc.ToString() + "_Position";
			data_logger6.Log(candidateId, experimentId, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), eventName, 
				(hitPos.x).ToString(),
				(hitPos.y).ToString(),
				(hitPos.z).ToString());
			data_logger6.StopLogging();
		}		
	}

	private void Update()
    {		
		// ray = new Ray(gazePointer.transform.position, gazePointer.transform.forward);
		ray = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
		
		RaycastHit hitData;
		if (Physics.Raycast(ray, out hitData) && !aButtonPressed) {
			string hitName = hitData.collider.gameObject.name;
			if ((hitName == "Stick") || (hitName == "HitStick")) {
				// Set the cursor position to the hit point and rotate based on the normal vector of the hit
				cursorInstance.transform.position = hitData.point;
				cursorInstance.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitData.normal);	
				
				// The Ray hit the Stick
				hitPosition1 = hitData.point;
				// hitPosition2 = hitData.normal;
				
				//gazePoint = hitData.point;
				//hitPosition3 = Camera.main.WorldToScreenPoint(gazePoint);	// Update the position of the gaze indicator UI element
				
				// Distance
				hitDistance = hitData.distance;
				
				// gameObject size
				Vector3 hitSizeCollider = hitData.collider.bounds.size;
				
				// Reads the Collider tag
				string tag = hitData.collider.tag;

				// Gets a Game Object reference from its Transform
				GameObject hitObject = hitData.transform.gameObject;
				
				// Get the Stick size (Room1: 0.49, 0.02, 0.02 / Room2: 0.52, 0.02, 0.39 / Room3: xxxxxxxxx)
				MeshRenderer renderer;
				renderer = hitObject.GetComponent<MeshRenderer>();
				Vector3 hitSizeRenderer = renderer.bounds.size;
				
				// Stick position - Static values (Room1: 13.62, 1.09, 22.66 / Room2: 2.89, 1.09, 22.69 / Room3: xxxxxxx )
				hitTransPosition = hitData.transform.position;

				//Debug.Log("Stick (size): " + hitSize);
				//Debug.Log("Stick (transform pos): " + hitTransPosition);
				//Debug.Log("Stick (point pos): " + hitPosition);
				
				OVRInput.Update(); // Call before checking the input		
				if (OVRInput.Get(OVRInput.RawButton.Y) && !aButtonPressed)
				{					
					aButtonPressed = true;
					
					// Write hit position to logfile
					data_logger1 = new DataLogger(";", "hitPosition_P" + tabLocation.ToString() + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
					data_logger1.StartLogging();		
					Write2Log1(tag,hitPosition1,hitDistance,tabLocation);				
					
					// Write Stick size (Collider) to logfile
/* 					data_logger2 = new DataLogger(";", "StickSizeCollider_C_P" + tabLocation.ToString() + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
					data_logger2.StartLogging();	
					Write2Log2(tag,hitSizeCollider,0,tabLocation); */

					// Write Stick size (Renderer) to logfile
/* 					data_logger3 = new DataLogger(";", "StickSizeRenderer_R_P" + tabLocation.ToString() + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
					data_logger3.StartLogging();	
					Write2Log3(tag,hitSizeRenderer,0,tabLocation); */
					
					// Write Stick position to logfile
					data_logger4 = new DataLogger(";", "StickTransPos_P" + tabLocation.ToString() + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
					data_logger4.StartLogging();	
					Write2Log4(tag,hitTransPosition,0,tabLocation);
					
					// Write hit position to logfile
/* 					data_logger5 = new DataLogger(";", "hitPosition_2_P" + tabLocation.ToString() + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
					data_logger5.StartLogging();		
					Write2Log5(tag,hitPosition2,hitDistance,tabLocation); */	
					
					// Write hit position to logfile
/* 					data_logger6 = new DataLogger(";", "hitPosition_3_P" + tabLocation.ToString() + "_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");
					data_logger6.StartLogging();		
					Write2Log6(tag,hitPosition3,hitDistance,tabLocation);	 */

					StartCoroutine(waiter());
				}
			}
		} else
		{
			// If the ray doesn't hit anything, set the cursor position to the maxCursorDistance and rotate to point away from the camera
			cursorInstance.transform.position = ray.origin + ray.direction.normalized * maxCursorDistance;
			cursorInstance.transform.rotation = Quaternion.FromToRotation(Vector3.up, -ray.direction);
		}
	}	
}
