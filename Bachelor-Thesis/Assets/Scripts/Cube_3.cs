using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube_3 : MonoBehaviour
{
    private Rigidbody m_Rigidbody;

    [SerializeField]
    public float m_Force = 20f;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Equals("LeftHandCollider"))
        {
            Vector3 force_direction = m_Rigidbody.transform.position - other.transform.position;
            force_direction.y = 0;
            force_direction.Normalize();
    
            m_Rigidbody.AddForceAtPosition(force_direction * m_Force, transform.position, ForceMode.Impulse);
        }

        if (other.name.Equals("RightHandCollider"))
        {
            Vector3 force_direction = m_Rigidbody.transform.position - other.transform.position;
            force_direction.y = 0;
            force_direction.Normalize();
    
            m_Rigidbody.AddForceAtPosition(force_direction * m_Force, transform.position, ForceMode.Impulse);
        }
    }
}
