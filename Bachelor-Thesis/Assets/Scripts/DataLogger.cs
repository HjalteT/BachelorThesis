using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

using UnityEngine;

/*
How to use the DataLogger class: 

public class MainClass
{
    public static void Main(string[] args)
    {
        DataLogger data_logger = new DataLogger(";", "PositionLog_", "UserId", "TrialId", "Timestamp", "Event", "PosX", "PosY", "PosZ");

        data_logger.StartLogging();
        data_logger.Log(0, 1, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), "PlayerPosition", (-0.1464635).ToString(CultureInfo.InvariantCulture), 1.ToString(CultureInfo.InvariantCulture), (-0.000312534).ToString(CultureInfo.InvariantCulture));
        data_logger.Log(0, 1, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), "PlayerPosition", (-0.1539453).ToString(CultureInfo.InvariantCulture), 1.ToString(CultureInfo.InvariantCulture), (-0.0009537541).ToString(CultureInfo.InvariantCulture));
        data_logger.Log(0, 1, DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(), "PlayerPosition", (-0.1616363).ToString(CultureInfo.InvariantCulture), 1.ToString(CultureInfo.InvariantCulture), (-0.001804266).ToString(CultureInfo.InvariantCulture));
        data_logger.StopLogging();
    }
}
*/

public class DataLogger : MonoBehaviour
{
    private string m_separator;
    private string m_filename_base;
    private string[] m_headers;

    private bool m_is_logging = false;
    private List<string>? m_rows = null;

    public DataLogger(string separator, string filename_base, params string[] headers)
    {
        if (headers.Length == 0)
        {
            throw new ArgumentException("Headers can not be of size 0.");
        }

        m_separator = separator;
        m_filename_base = filename_base;
        m_headers = headers;
    }

    public void StartLogging()
    {
        if (m_is_logging)
        {
            return;
        }

        m_rows = new List<string>();
        m_is_logging = true;
    }

    // FIXME: The instructions are contradictory.
    // It says, "...whenever Log(x, y, z) is called the DataLogger converts the x,y,z values to strings..."
    // but then it says that Log must takes these parameters including "params string[] items".
    // So which is it? Do we convert the values before giving them to Log or if Log handles the converstion
    // the type must be changed for "items".
    public void Log(int userId, int trialId, long timestamp, string eventName, params string[] items)
    {
        if (!(m_is_logging) || m_rows == null)
        {
            return;
        }

        // If the number of arguments passed in does not equal how many headers we have, return.
        if ((items.Length + 4) != m_headers.Length)
        {
            return;
        }

        string row = $"{userId}{m_separator}{trialId}{m_separator}{timestamp}{m_separator}{eventName}";

        foreach(string item in items)
        {
            row += $"{m_separator}{item}";
        }

        m_rows.Add(row);
    }

    public void StopLogging()
    {
        if (!(m_is_logging) || m_rows == null)
        {
            return;
        }

        string filepath = Application.persistentDataPath + "/" + m_filename_base + System.DateTime.Now.ToString("ddmmyyyy-HHmmss") + ".csv";
        string header_line = $"{m_headers[0]}";


        foreach (string header in m_headers.Skip(1))
        {
            header_line += $"{m_separator}{header}";
        }

        using (StreamWriter file = new StreamWriter(filepath))
        {
            file.WriteLine(header_line);

            foreach (string row in m_rows)
            {
                file.WriteLine(row);
            }
        }

        m_is_logging = false;
    }
}
