using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetCoordinates : MonoBehaviour
{
    public Transform stick; // Reference to the stick you're interacting with
    public GameObject gazeIndicator; // Reference to the UI Image for the gaze indicator

    private bool capturingCoordinates = false;
    private Vector3 gazePoint; // Stores the point where the user is looking

    private List<Vector3> capturedCoordinates = new List<Vector3>(); // Store captured coordinates

    private void Update()
    {
        // Perform raycasting when the capturingCoordinates flag is true
        if (capturingCoordinates)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
            {
                if (hit.collider.gameObject == stick.gameObject)
                {
                    gazePoint = hit.point;

                    // Update the position of the gaze indicator UI element
                    Vector3 screenPoint = Camera.main.WorldToScreenPoint(gazePoint);
                    gazeIndicator.transform.position = screenPoint;
                }
            }
        }
    }

    // Called when the button is pressed
    public void StartCaptureCoordinates()
    {
        capturedCoordinates.Clear(); // Clear previous captured coordinates
        capturingCoordinates = true;
    }

    // Called when the button is released
    public void StopCaptureCoordinates()
    {
        capturingCoordinates = false;

        // Store the captured gazePoint in the list
        capturedCoordinates.Add(gazePoint);
    }

    // Save captured coordinates to a file or data structure
    public void SaveCapturedCoordinatesToFile(string filename)
    {
        string filePath = Application.dataPath + "/" + filename + ".txt";
        using (System.IO.StreamWriter file = new System.IO.StreamWriter(filePath))
        {
            foreach (Vector3 coord in capturedCoordinates)
            {
                file.WriteLine(coord.x + "," + coord.y + "," + coord.z);
                Debug.Log(coord.x + "," + coord.y + "," + coord.z);
            }
        }
    }
}
