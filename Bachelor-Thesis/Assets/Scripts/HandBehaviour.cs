using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandBehaviour : MonoBehaviour
{

    public Transform playerLeftHand;
    public Transform playerRightHand;
    // Update is called once per frame

    //void Start()
    //{
    //    var hand = GetComponent<OVRHand>();
    //}


    void Update()
    {        
        // Synchronize the position and rotation of the extra hands with the player's hands
        if (playerLeftHand != null)
        {
            transform.Find("OVRPlugin.Hand.HandLeft").SetPositionAndRotation(playerLeftHand.position, playerLeftHand.rotation); 
        }

        if (playerRightHand != null)
        {
            transform.Find("OVRPlugin.Hand.HandRight").SetPositionAndRotation(playerRightHand.position, playerRightHand.rotation); 
        }
    }
}
