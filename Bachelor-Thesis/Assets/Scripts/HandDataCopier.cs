using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandDataCopier : MonoBehaviour
{

    public Transform inputLeftHand;
    public Transform inputRightHand;
    public Transform outputLeftHand;
    public Transform outputRightHand;

    void Update()
    {
        outputLeftHand.position = inputLeftHand.position;
        outputLeftHand.rotation = inputLeftHand.rotation;

        outputRightHand.position = inputRightHand.position;
        outputRightHand.rotation = inputRightHand.rotation;
    }
}
