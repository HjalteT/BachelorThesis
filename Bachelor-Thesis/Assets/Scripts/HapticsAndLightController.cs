using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HapticsAndLightController : MonoBehaviour
{
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
	// IMPORTANT: Select a value from 1-4 depending of the experiment to conduct.
	// ID=1: non-CRI without Stick2
	// ID=2: CRI without Stick2
	// ID=3: non-CRI with Stick2
	// ID=4: CRI with Stick2
	private int experimentId = 1;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public GameObject lightLeft;
    public GameObject lightMid;
    public GameObject lightRight;
	private GameObject activeObject;	
	
	private bool aButtonPressed = false;
	private bool showHitStick = false;			// DO NOT CHANGE THIS DEFAULT VALUE!

	private bool nonCRI_hideStick2 = false;		// ID=1 -> non-CRI-ISI: Left -> Right and without 'HitStick'
	private bool CRI_hideStick2 = false;		// ID=2 -> CRI-ISI: Left -> Right and without 'HitStick'
	private bool nonCRI_wStick2 = false;		// ID=3 -> non-CRI-ISI: Left -> Right and with 'HitStick'
	private bool CRI_wStick2 = false;			// ID=4 -> CRI-ISI: Left -> Right and with 'HitStick'

	// Haptic with Light function
	private IEnumerator HapticsAndLightSequence(float frequency, float amplitude, float durationStart, float durationP1P2, float durationP2P3, float durationHaptic, bool left2right)
    {
		//
        // CRI ISIs (P1-P2: 800ms, P2-P3: 80ms, haptic vibration: 60ms)
		//

        if (aButtonPressed == true)
        {
			// Wait a few seconds before we start to prepare the test candidates
			yield return new WaitForSeconds(durationStart);

			// ######### P1: #########
			// Turn on light (P1)
			if (left2right) lightLeft.SetActive(true);
				else lightRight.SetActive(true);

			// Send vibration (P1) through the controller 1
			if (left2right) OVRInput.SetControllerVibration(frequency, amplitude, OVRInput.Controller.LTouch);
				else OVRInput.SetControllerVibration(frequency, amplitude, OVRInput.Controller.RTouch);

			// allow Haptics feedback in controller for defined time and stop
			yield return new WaitForSeconds(durationHaptic);
			if (left2right) OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);
				else OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);

			// Turn off light (P1)
			if (left2right) lightLeft.SetActive(false);
				else lightRight.SetActive(false);

			// delay between P1 and P2
			yield return new WaitForSeconds(durationP1P2);


			// ######### P2: #########
            // Turn on light (P2)
            lightMid.SetActive(true);
			
			// Send vibration (P2) through the controller 1
			if (left2right) OVRInput.SetControllerVibration(frequency, amplitude, OVRInput.Controller.LTouch);
				else OVRInput.SetControllerVibration(frequency, amplitude, OVRInput.Controller.RTouch);

			// allow Haptics feedback in controller for defined time and stop
			yield return new WaitForSeconds(durationHaptic);
			if (left2right) OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);
				else OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);

            // Turn off light (P2)
            lightMid.SetActive(false);

			// delay between P2 and P3
			yield return new WaitForSeconds(durationP2P3);
			
			
			// ######### P3: #########
			// Turn on light (P3)
			if (left2right) lightRight.SetActive(true);
				else lightLeft.SetActive(true);
			
			// Send vibration (P3) through the controller 2
			if (left2right) OVRInput.SetControllerVibration(frequency, amplitude, OVRInput.Controller.RTouch);
				else OVRInput.SetControllerVibration(frequency, amplitude, OVRInput.Controller.LTouch);			

			// allow Haptics feedback in controller for defined time and stop
			yield return new WaitForSeconds(durationHaptic);
			if (left2right) OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);
				else OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);

            // Turn off light (P3)
			if (left2right) lightRight.SetActive(false);
				else lightLeft.SetActive(false);

            aButtonPressed = false;
        }
    }
		
	void Start()
	{
		nonCRI_hideStick2 = false;
		CRI_hideStick2 = false;
		nonCRI_wStick2 = false;
		CRI_wStick2 = false;
		if (experimentId == 1)
			nonCRI_hideStick2 = true;	// ID=1 -> non-CRI-ISI: Left -> Right and without 'HitStick'
		if (experimentId == 2)
			CRI_hideStick2 = true;		// ID=2 -> CRI-ISI: Left -> Right and without 'HitStick'
		if (experimentId == 3)
			nonCRI_wStick2 = true;		// ID=3 -> non-CRI-ISI: Left -> Right and with 'HitStick'
		if (experimentId == 4)
			CRI_wStick2 = true;			// ID=4 -> CRI-ISI: Left -> Right and with 'HitStick'	
		
		// Hide/Unhide "Stick 2" depending on choosen experiment
		if (nonCRI_hideStick2 || CRI_hideStick2) 
		{
			showHitStick = false;
		} else 
		{
			showHitStick = true;
		}
		activeObject = GameObject.FindGameObjectWithTag("R2S2");			// Room 2 Stick 2 (fake stick)
		if (activeObject != null)
			activeObject.GetComponent<Renderer>().enabled = showHitStick;
		
		activeObject = GameObject.FindGameObjectWithTag("R3S2");			// Room 3 Stick 2 (fake stick)
		if (activeObject != null)
			activeObject.GetComponent<Renderer>().enabled = showHitStick;
	}

    private void Update()
    {
		OVRInput.Update(); // Call before checking the input
        if (OVRInput.Get(OVRInput.RawButton.X) && !aButtonPressed)
        {
            aButtonPressed = true;
			
			// CRI-ISIs
			if (CRI_hideStick2 || CRI_wStick2) StartCoroutine(HapticsAndLightSequence(1, 1, 5f, 0.8f, 0.08f, 0.06f, true));

			// non-CRI-ISIs
			if (nonCRI_hideStick2 || nonCRI_wStick2) StartCoroutine(HapticsAndLightSequence(1, 1, 5f, 0.08f, 0.8f, 0.06f, true));
        }
    }
}
