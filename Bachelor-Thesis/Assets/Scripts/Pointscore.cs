using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Pointscore : MonoBehaviour
{
    // Start is called before the first frame update
    public int AddToScore;
    public string Color;
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Throwable")
        {
            Global.lars_score += AddToScore;
            Debug.Log("Scored a point");
            Debug.Log(Color + " " + Global.lars_score);
        }
    }
}
