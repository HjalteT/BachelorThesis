using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ShrinkObjectVr : MonoBehaviour
{

    public GameObject button;
    public UnityEvent onPress;
    public UnityEvent onRelease;
    GameObject presser;
    bool isPressed;

    void Start()
    {
        isPressed = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isPressed)
        {
            button.transform.localPosition = new Vector3((float)0.196, (float)0.040, (float)0.064);
            presser = other.gameObject;
            onPress.Invoke();
            isPressed = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other == presser)
        {
            button.transform.localPosition = new Vector3((float)0.196, (float)0.047, (float)0.064);
            onRelease.Invoke();
            isPressed = false;
        }
    }

    public void Shrink()
    {
        //This should somehow shrink the objects for the game however i couldnt figure out how to apply it to them
        //transform.localScale = new Vector3((float)0.16,0.6,0.4)
    }

}