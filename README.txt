In order to try the experiment you first have to know the controls:

- Pressing X starts the vibro-tactile stimuli

- Pressing Y will log the coordinate on the stick you are looking at

- Pressing A will change room

- Pressing B will give you access to the questionnaire

To be able to distinguish more easily between the experiments and participants, it's highly recommended to change the experimentID and participantID, before each execution.
The below four files needs to be modified, all in the beginning of the file.

.\Bachelor-Thesis\Assets\Scripts\
                      HapticsAndLightController.cs
                      BachelorExperiment.cs
                      ControllerLogging.cs

Questionnaire:
.\Bachelor-Thesis\Assets\Questionnaires\Scripts\Export
                      ExportToCSV.cs
